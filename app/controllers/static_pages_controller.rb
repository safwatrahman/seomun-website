class StaticPagesController < ApplicationController
  def home
  end

  def conference
  end

  def leadership
  end

  def registration
  end

  def contactus
  end
  
  def aboutus
  end
  
  def committees
  end
  
  def schedule 
  end
  
  def location
  end 
  
  def hotels 
  end 
  
  def tourinfo 
  end 
  
  def secretarygeneral 
  end 
  
  def deputysecretarygeneral
  end
  
  def admin
  end
  
  def tech
  end 
  
  def chairapps
  end 
  
  def school 
  end 
  
  def banking 
  end 
  
  def teacherrecs
  end 
  
  def hotels
  end 
  
  def schoolregis
  end 
  
  def ga1
  end 
  
  def ga2
  end 
  
  def ecosoc1
  end 
  
  def ecosoc2
  end 
  
  def hrc1
  end 
  
  def hrc2 
  end 
  
  def ec
  end 
  
  def spc
  end 
  
  def spt
  end 
  
  def ap 
  end 
  
  def disarm
  end 
  
  def sc 
  end 
  
  def icj
  end 
  
  def hissc
  end 
  
  def icjregistration
  end
  
  def transportation
  end 
  
  def photos
  end 
  
end
