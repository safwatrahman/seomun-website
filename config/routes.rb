Rails.application.routes.draw do
  root 'static_pages#home'

  get 'conference' => 'static_pages#conference'

  get 'leadership' => 'static_pages#leadership'

  get 'registration' => 'static_pages#registration'

  get 'contactus' => 'static_pages#contactus'
  
  get 'tourinfo' => 'static_pages#tourinfo'
  
  get 'aboutus' => 'static_pages#aboutus'
  
  get 'committees' => 'static_pages#committees'
  
  get 'location' => 'static_pages#location'
    
  get 'schedule' => 'static_pages#schedule'
  
  get 'secretarygeneral' => 'static_pages#secretarygeneral'
  
  get 'deputysecretarygeneral' => 'static_pages#deputysecretarygeneral'
  
  get 'admin' => 'static_pages#admin'
  
  get 'tech' => 'static_pages#tech'
  
  get 'chairapps' => 'static_pages#chairapps'
  
  get 'school' => 'static_pages#school'
  
  get 'banking'=> 'static_pages#banking'
  
  get 'teacherrecs' => 'static_pages#teacherrecs'
  
  get 'hotel' => 'static_pages#hotel'
  
  get 'schoolregis' => 'static_pages#schoolregis'
  
  get 'ga1' => 'static_pages#ga1'
  
  get 'ga2' => 'static_pages#ga2'
  
  get 'ecosoc1' => 'static_pages#ecosoc1'
  
  get 'ecosoc2' => 'static_pages#ecosoc2'
  
  get 'hrc1' => 'static_pages#hrc1'
  
  get 'hrc2' => 'static_pages#hrc2'
  
  get 'ec' => 'static_pages#ec'
  
  get 'spc' => 'static_pages#spc'
  
  get 'spt' => 'static_pages#spt'
  
  get 'ap' => 'static_pages#ap'
  
  get 'disarm' => 'static_pages#disarm'
  
  get 'sc' => 'static_pages#sc'
  
  get 'icj' => 'static_pages#icj'
  
  get 'hissc' => 'static_pages#hissc'
  
  get 'icjregistration' => 'static_pages#icjregistration'
  
  get 'transportation' => 'static_pages#transportation'
  
  get 'photos' => 'static_pages#photos'
  # The priority is based upon order of creation: first created -> highesiority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
