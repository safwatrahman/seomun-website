require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
  end

  test "should get conference" do
    get :conference
    assert_response :success
  end

  test "should get leadership" do
    get :leadership
    assert_response :success
  end

  test "should get registration" do
    get :registration
    assert_response :success
  end

  test "should get contactus" do
    get :contactus
    assert_response :success
  end

end
